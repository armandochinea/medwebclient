﻿using DevExpress.Persistent.BaseImpl.PermissionPolicy;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEDWebClient.Module.BusinessObjects.MobileMED
{
	public class SystemUser : PermissionPolicyUser
	{
		public SystemUser(Session session) : base(session) { }
		public override void AfterConstruction() { base.AfterConstruction(); }

		#region Properties
		string fName;
		public string Name
		{
			get { return fName; }
			set { SetPropertyValue("Name", ref fName, value); }
		}
		#endregion
	}
}
