﻿using System;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;

namespace MEDWebClient.Module.BusinessObjects.MobileMED
{

	public partial class City
	{
		public City(Session session) : base(session) { }
		public override void AfterConstruction() { base.AfterConstruction(); }

		#region Properties
		SystemUser fCreatedBy;
		public SystemUser CreatedBy
		{
			get { return fCreatedBy; }
			set { SetPropertyValue("CreatedBy", ref fCreatedBy, value); }
		}

		DateTime fCreatedOn;
		public DateTime CreatedOn
		{
			get { return fCreatedOn; }
			set { SetPropertyValue("CreatedOn", ref fCreatedOn, value); }
		}

		SystemUser fModifiedBy;
		public SystemUser ModifiedBy
		{
			get { return fModifiedBy; }
			set { SetPropertyValue("ModifiedBy", ref fModifiedBy, value); }
		}

		DateTime fModifiedOn;
		public DateTime ModifiedOn
		{
			get { return fModifiedOn; }
			set { SetPropertyValue("ModifiedOn", ref fModifiedOn, value); }
		}
		#endregion
	}

}
