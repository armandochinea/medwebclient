﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
namespace MEDWebClient.Module.BusinessObjects.MobileMED
{

	[Persistent(@"Medics")]
	public partial class Medic : XPLiteObject
	{
		long fID;
		[Key(true)]
		public long ID
		{
			get { return fID; }
			set { SetPropertyValue<long>(nameof(ID), ref fID, value); }
		}
		string fName;
		[ColumnDefaultValue("")]
		public string Name
		{
			get { return fName; }
			set { SetPropertyValue<string>(nameof(Name), ref fName, value); }
		}
		string fLastName;
		[ColumnDefaultValue("")]
		public string LastName
		{
			get { return fLastName; }
			set { SetPropertyValue<string>(nameof(LastName), ref fLastName, value); }
		}
		string fLicense;
		[ColumnDefaultValue("")]
		public string License
		{
			get { return fLicense; }
			set { SetPropertyValue<string>(nameof(License), ref fLicense, value); }
		}
		Specialty fSpecialty;
		public Specialty Specialty
		{
			get { return fSpecialty; }
			set { SetPropertyValue<Specialty>(nameof(Specialty), ref fSpecialty, value); }
		}
		Specialty fSubSpecialty;
		public Specialty SubSpecialty
		{
			get { return fSubSpecialty; }
			set { SetPropertyValue<Specialty>(nameof(SubSpecialty), ref fSubSpecialty, value); }
		}
		long fRefID;
		[ColumnDefaultValue(0)]
		public long RefID
		{
			get { return fRefID; }
			set { SetPropertyValue<long>(nameof(RefID), ref fRefID, value); }
		}
		MedicClass fClass;
		[ColumnDefaultValue("")]
		public MedicClass Class
		{
			get { return fClass; }
			set { SetPropertyValue<MedicClass>(nameof(Class), ref fClass, value); }
		}
		string fEmail;
		[ColumnDefaultValue("")]
		public string Email
		{
			get { return fEmail; }
			set { SetPropertyValue<string>(nameof(Email), ref fEmail, value); }
		}
		DateTime fBirthDate;
		public DateTime BirthDate
		{
			get { return fBirthDate; }
			set { SetPropertyValue<DateTime>(nameof(BirthDate), ref fBirthDate, value); }
		}
		string fDEALicense;
		[ColumnDefaultValue("")]
		public string DEALicense
		{
			get { return fDEALicense; }
			set { SetPropertyValue<string>(nameof(DEALicense), ref fDEALicense, value); }
		}
		DateTime fDEAExpDate;
		public DateTime DEAExpDate
		{
			get { return fDEAExpDate; }
			set { SetPropertyValue<DateTime>(nameof(DEAExpDate), ref fDEAExpDate, value); }
		}
		string fDMLicense;
		[ColumnDefaultValue("")]
		public string DMLicense
		{
			get { return fDMLicense; }
			set { SetPropertyValue<string>(nameof(DMLicense), ref fDMLicense, value); }
		}
		DateTime fDMExpDate;
		public DateTime DMExpDate
		{
			get { return fDMExpDate; }
			set { SetPropertyValue<DateTime>(nameof(DMExpDate), ref fDMExpDate, value); }
		}
		[Association(@"Medic_OfficeReferencesMedic")]
		public XPCollection<Medic_Office> Medic_Offices { get { return GetCollection<Medic_Office>(nameof(Medic_Offices)); } }
		[Association(@"Route_MedicReferencesMedic")]
		public XPCollection<Route_Medic> Route_Medics { get { return GetCollection<Route_Medic>(nameof(Route_Medics)); } }
	}

}
