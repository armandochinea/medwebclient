﻿using System;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using MEDWebClient.Module.Helpers;

namespace MEDWebClient.Module.BusinessObjects.MobileMED
{

	public partial class Office
	{
		public Office(Session session) : base(session) { }
		public override void AfterConstruction() { base.AfterConstruction(); }

		#region Properties
		Enums.Status fStatus;
		public Enums.Status Status
		{
			get { return fStatus; }
			set { SetPropertyValue("Status", ref fStatus, value); }
		}

		Enums.TransStatus fTransStatus;
		public Enums.TransStatus TransStatus
		{
			get { return fTransStatus; }
			set { SetPropertyValue("TransStatus", ref fTransStatus, value); }
		}

		SystemUser fCreatedBy;
		public SystemUser CreatedBy
		{
			get { return fCreatedBy; }
			set { SetPropertyValue("CreatedBy", ref fCreatedBy, value); }
		}

		DateTime fCreatedOn;
		public DateTime CreatedOn
		{
			get { return fCreatedOn; }
			set { SetPropertyValue("CreatedOn", ref fCreatedOn, value); }
		}

		SystemUser fModifiedBy;
		public SystemUser ModifiedBy
		{
			get { return fModifiedBy; }
			set { SetPropertyValue("ModifiedBy", ref fModifiedBy, value); }
		}

		DateTime fModifiedOn;
		public DateTime ModifiedOn
		{
			get { return fModifiedOn; }
			set { SetPropertyValue("ModifiedOn", ref fModifiedOn, value); }
		}
		#endregion
	}

}
