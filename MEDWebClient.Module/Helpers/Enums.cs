﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEDWebClient.Module.Helpers
{
	public class Enums
	{
		public enum Status
		{
			New = -1,
			Inactive = 0,
			Active = 1,
			Deleted = 2
		}

		public enum TransStatus
		{
			Created = 0,
			Locked = 1,
			SentToServer = 2,
			LockedForExport = 3,
			ErrorOnExport = 4,
			SentToERP = 5
		}

		public enum OfficeType
		{
			Private = 1,
			Shared = 2
		}
	}
}
