﻿using System;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.Web;
using System.Collections.Generic;
using DevExpress.ExpressApp.Xpo;
using DevExpress.ExpressApp.Security;
using DevExpress.ExpressApp.Security.ClientServer;

namespace MEDWebClient.Web {
    // For more typical usage scenarios, be sure to check out https://docs.devexpress.com/eXpressAppFramework/DevExpress.ExpressApp.Web.WebApplication
    public partial class MEDWebClientAspNetApplication : WebApplication {
        private DevExpress.ExpressApp.SystemModule.SystemModule module1;
        private DevExpress.ExpressApp.Web.SystemModule.SystemAspNetModule module2;
        private MEDWebClient.Module.MEDWebClientModule module3;
        private MEDWebClient.Module.Web.MEDWebClientAspNetModule module4;
        private DevExpress.ExpressApp.Security.SecurityModule securityModule1;
        private DevExpress.ExpressApp.Security.SecurityStrategyComplex securityStrategyComplex1;
        private DevExpress.ExpressApp.Security.AuthenticationStandard authenticationStandard1;
        private DevExpress.ExpressApp.Objects.BusinessClassLibraryCustomizationModule objectsModule;
        private DevExpress.ExpressApp.Chart.ChartModule chartModule;
        private DevExpress.ExpressApp.Chart.Web.ChartAspNetModule chartAspNetModule;
        private DevExpress.ExpressApp.CloneObject.CloneObjectModule cloneObjectModule;
        private DevExpress.ExpressApp.ConditionalAppearance.ConditionalAppearanceModule conditionalAppearanceModule;
        private DevExpress.ExpressApp.Dashboards.DashboardsModule dashboardsModule;
        private DevExpress.ExpressApp.Dashboards.Web.DashboardsAspNetModule dashboardsAspNetModule;
        private DevExpress.ExpressApp.Maps.Web.MapsAspNetModule mapsAspNetModule;
        private DevExpress.ExpressApp.Notifications.NotificationsModule notificationsModule;
        private DevExpress.ExpressApp.Notifications.Web.NotificationsAspNetModule notificationsAspNetModule;
        private DevExpress.ExpressApp.ReportsV2.ReportsModuleV2 reportsModuleV2;
        private DevExpress.ExpressApp.ReportsV2.Web.ReportsAspNetModuleV2 reportsAspNetModuleV2;
        private DevExpress.ExpressApp.Validation.ValidationModule validationModule;
        private DevExpress.ExpressApp.Validation.Web.ValidationAspNetModule validationAspNetModule;

        #region Default XAF configuration options (https://www.devexpress.com/kb=T501418)
        static MEDWebClientAspNetApplication() {
			EnableMultipleBrowserTabsSupport = true;
            DevExpress.ExpressApp.Web.Editors.ASPx.ASPxGridListEditor.AllowFilterControlHierarchy = true;
            DevExpress.ExpressApp.Web.Editors.ASPx.ASPxGridListEditor.MaxFilterControlHierarchyDepth = 3;
            DevExpress.ExpressApp.Web.Editors.ASPx.ASPxCriteriaPropertyEditor.AllowFilterControlHierarchyDefault = true;
            DevExpress.ExpressApp.Web.Editors.ASPx.ASPxCriteriaPropertyEditor.MaxHierarchyDepthDefault = 3;
            DevExpress.Persistent.Base.PasswordCryptographer.EnableRfc2898 = true;
            DevExpress.Persistent.Base.PasswordCryptographer.SupportLegacySha512 = false;
        }
        private void InitializeDefaults() {
            LinkNewObjectToParentImmediately = false;
            OptimizedControllersCreation = true;
        }
        #endregion
        public MEDWebClientAspNetApplication() {
            InitializeComponent();
			InitializeDefaults();
        }
        protected override IViewUrlManager CreateViewUrlManager() {
            return new ViewUrlManager();
        }
        protected override void CreateDefaultObjectSpaceProvider(CreateCustomObjectSpaceProviderEventArgs args) {
            args.ObjectSpaceProvider = new SecuredObjectSpaceProvider((SecurityStrategyComplex)Security, GetDataStoreProvider(args.ConnectionString, args.Connection), true);
            args.ObjectSpaceProviders.Add(new NonPersistentObjectSpaceProvider(TypesInfo, null));
        }
        private IXpoDataStoreProvider GetDataStoreProvider(string connectionString, System.Data.IDbConnection connection) {
            System.Web.HttpApplicationState application = (System.Web.HttpContext.Current != null) ? System.Web.HttpContext.Current.Application : null;
            IXpoDataStoreProvider dataStoreProvider = null;
            if(application != null && application["DataStoreProvider"] != null) {
                dataStoreProvider = application["DataStoreProvider"] as IXpoDataStoreProvider;
            }
            else {
                dataStoreProvider = XPObjectSpaceProvider.GetDataStoreProvider(connectionString, connection, true);
                if(application != null) {
                    application["DataStoreProvider"] = dataStoreProvider;
                }
            }
			return dataStoreProvider;
        }
        private void MEDWebClientAspNetApplication_DatabaseVersionMismatch(object sender, DevExpress.ExpressApp.DatabaseVersionMismatchEventArgs e) {
#if EASYTEST
            e.Updater.Update();
            e.Handled = true;
#else
            if(System.Diagnostics.Debugger.IsAttached) {
                e.Updater.Update();
                e.Handled = true;
            }
            else {
				string message = "The application cannot connect to the specified database, " +
					"because the database doesn't exist, its version is older " +
					"than that of the application or its schema does not match " +
					"the ORM data model structure. To avoid this error, use one " +
					"of the solutions from the https://www.devexpress.com/kb=T367835 KB Article.";

                if(e.CompatibilityError != null && e.CompatibilityError.Exception != null) {
                    message += "\r\n\r\nInner exception: " + e.CompatibilityError.Exception.Message;
                }
                throw new InvalidOperationException(message);
            }
#endif
        }
        private void InitializeComponent() {
			this.module1 = new DevExpress.ExpressApp.SystemModule.SystemModule();
			this.module2 = new DevExpress.ExpressApp.Web.SystemModule.SystemAspNetModule();
			this.module3 = new MEDWebClient.Module.MEDWebClientModule();
			this.module4 = new MEDWebClient.Module.Web.MEDWebClientAspNetModule();
			this.securityModule1 = new DevExpress.ExpressApp.Security.SecurityModule();
			this.securityStrategyComplex1 = new DevExpress.ExpressApp.Security.SecurityStrategyComplex();
			this.authenticationStandard1 = new DevExpress.ExpressApp.Security.AuthenticationStandard();
			this.objectsModule = new DevExpress.ExpressApp.Objects.BusinessClassLibraryCustomizationModule();
			this.chartModule = new DevExpress.ExpressApp.Chart.ChartModule();
			this.chartAspNetModule = new DevExpress.ExpressApp.Chart.Web.ChartAspNetModule();
			this.cloneObjectModule = new DevExpress.ExpressApp.CloneObject.CloneObjectModule();
			this.conditionalAppearanceModule = new DevExpress.ExpressApp.ConditionalAppearance.ConditionalAppearanceModule();
			this.dashboardsModule = new DevExpress.ExpressApp.Dashboards.DashboardsModule();
			this.dashboardsAspNetModule = new DevExpress.ExpressApp.Dashboards.Web.DashboardsAspNetModule();
			this.mapsAspNetModule = new DevExpress.ExpressApp.Maps.Web.MapsAspNetModule();
			this.notificationsModule = new DevExpress.ExpressApp.Notifications.NotificationsModule();
			this.notificationsAspNetModule = new DevExpress.ExpressApp.Notifications.Web.NotificationsAspNetModule();
			this.reportsModuleV2 = new DevExpress.ExpressApp.ReportsV2.ReportsModuleV2();
			this.reportsAspNetModuleV2 = new DevExpress.ExpressApp.ReportsV2.Web.ReportsAspNetModuleV2();
			this.validationModule = new DevExpress.ExpressApp.Validation.ValidationModule();
			this.validationAspNetModule = new DevExpress.ExpressApp.Validation.Web.ValidationAspNetModule();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// securityStrategyComplex1
			// 
			this.securityStrategyComplex1.AllowAnonymousAccess = false;
			this.securityStrategyComplex1.Authentication = this.authenticationStandard1;
			this.securityStrategyComplex1.RoleType = typeof(DevExpress.Persistent.BaseImpl.PermissionPolicy.PermissionPolicyRole);
			this.securityStrategyComplex1.SupportNavigationPermissionsForTypes = false;
			this.securityStrategyComplex1.UserType = typeof(DevExpress.Persistent.BaseImpl.PermissionPolicy.PermissionPolicyUser);
			// 
			// authenticationStandard1
			// 
			this.authenticationStandard1.LogonParametersType = typeof(DevExpress.ExpressApp.Security.AuthenticationStandardLogonParameters);
			// 
			// cloneObjectModule
			// 
			this.cloneObjectModule.ClonerType = null;
			// 
			// dashboardsModule
			// 
			this.dashboardsModule.DashboardDataType = typeof(DevExpress.Persistent.BaseImpl.DashboardData);
			// 
			// notificationsModule
			// 
			this.notificationsModule.CanAccessPostponedItems = false;
			this.notificationsModule.NotificationsRefreshInterval = System.TimeSpan.Parse("00:05:00");
			this.notificationsModule.NotificationsStartDelay = System.TimeSpan.Parse("00:00:05");
			this.notificationsModule.ShowDismissAllAction = false;
			this.notificationsModule.ShowNotificationsWindow = true;
			this.notificationsModule.ShowRefreshAction = false;
			// 
			// reportsModuleV2
			// 
			this.reportsModuleV2.EnableInplaceReports = true;
			this.reportsModuleV2.ReportDataType = typeof(DevExpress.Persistent.BaseImpl.ReportDataV2);
			this.reportsModuleV2.ReportStoreMode = DevExpress.ExpressApp.ReportsV2.ReportStoreModes.XML;
			// 
			// reportsAspNetModuleV2
			// 
			this.reportsAspNetModuleV2.ReportViewerType = DevExpress.ExpressApp.ReportsV2.Web.ReportViewerTypes.HTML5;
			// 
			// validationModule
			// 
			this.validationModule.AllowValidationDetailsAccess = true;
			this.validationModule.IgnoreWarningAndInformationRules = false;
			// 
			// MEDWebClientAspNetApplication
			// 
			this.ApplicationName = "MEDWebClient";
			this.CheckCompatibilityType = DevExpress.ExpressApp.CheckCompatibilityType.DatabaseSchema;
			this.Modules.Add(this.module1);
			this.Modules.Add(this.module2);
			this.Modules.Add(this.objectsModule);
			this.Modules.Add(this.chartModule);
			this.Modules.Add(this.cloneObjectModule);
			this.Modules.Add(this.conditionalAppearanceModule);
			this.Modules.Add(this.dashboardsModule);
			this.Modules.Add(this.notificationsModule);
			this.Modules.Add(this.reportsModuleV2);
			this.Modules.Add(this.validationModule);
			this.Modules.Add(this.module3);
			this.Modules.Add(this.chartAspNetModule);
			this.Modules.Add(this.dashboardsAspNetModule);
			this.Modules.Add(this.mapsAspNetModule);
			this.Modules.Add(this.notificationsAspNetModule);
			this.Modules.Add(this.reportsAspNetModuleV2);
			this.Modules.Add(this.validationAspNetModule);
			this.Modules.Add(this.module4);
			this.Modules.Add(this.securityModule1);
			this.Security = this.securityStrategyComplex1;
			this.DatabaseVersionMismatch += new System.EventHandler<DevExpress.ExpressApp.DatabaseVersionMismatchEventArgs>(this.MEDWebClientAspNetApplication_DatabaseVersionMismatch);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
    }
}
